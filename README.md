# HDCore - docker-gitlab-runner

## Introduction

This is a basic docker-compose configuration to register and run gitlab runner in a local docker instance and can be linked to the online gitlab.com instance or a local gitlab instance.

## Registration

1. Certificates

If your gitlab instance use certificates from a local CA you have to add the exported certificate by placing the .crt file in the folder ./config/certs/ . **Attention**: the servername must be a fqdn hostname.domain.ext .
```bash
echo -n | openssl s_client -connect <servername>:<portname> | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > ./config/certs/<servername>.crt
```

2. Registration token

Go to the settings of your gitlab project. Choose for CI/CD and look in the Runners section for a project registration token.

3. Initial configuration

Create a .env file and complete the variables.

4. First registration

Execute this command once to register your local runner.

```bash
docker compose -f ./docker-compose-register.yml run --rm gitlab-runner-docker-register register
```

4. Configuration file

As a reference the created configuration file can be found at ./config/gitlab-runner/config.toml . This file is kept outside a docker volume so the project can easily be removed/moved.

5. Pull policy

To handle network issues during pulling the images add a fallback method [runners.docker].
```
pull_policy = ["always", "if-not-present"]
```

6. Docker-in-docker (optional)

If you use docker-in-docker (dind) you need to add an extra directory to the volumes parameter in the configuration file in [runners.docker].
```
volumes = ["xxxxxx", "/certs/client"]
```

## Execution

1. Starting the runner

```bash
docker compose up -d
```

2. Stopping/cleaning the runner

```bash
docker compose down
```

3. Update the runner

```bash
docker compose pull
docker compose up -d
```